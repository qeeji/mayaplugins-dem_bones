@echo off

cd ..
set "cmake=cmake"
set MAYA_PLUGIN_BUILD_DIR=%cd%
mkdir bin
set MAYA_PLUGIN_OUT_DIR=bin

REM Build the plugin
REM call :build 2018
REM call :build 2019
call :build 2020

exit /B

:build
echo "Compiling for Maya %~1"
rmdir /S /Q build_%~1
mkdir build_%~1
cd build_%~1
call :cmake_strict -G "Visual Studio 15 2017 Win64" -DMAYA_VERSION=%~1 ..
call :cmake_strict --build . --config Release --target clean
call :cmake_strict --build . --config Release
cd %MAYA_PLUGIN_BUILD_DIR%
mkdir %MAYA_PLUGIN_OUT_DIR%\%~1
copy build_%~1\src\Release\DemBones.mll %MAYA_PLUGIN_OUT_DIR%\%~1\DemBones.mll /y
rmdir /S /Q build_%~1
exit /B 0

:cmake_strict
"%cmake%" %*
if %errorlevel% equ 0 exit /B 0
echo ====================================================
echo Fatal error with cmake - error code: %errorlevel%
echo ====================================================
exit %errorlevel%
